const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const cookieParser = require("cookie-parser");
const dotenv = require("dotenv");
const authRoute = require("./routes/auth");
const userRoute = require("./routes/user");
dotenv.config();

const app = express();
app.use(
  cors({
    origin: "http://localhost:3000",
    credentials: true,
    withCredentials: true,
  })
);
app.use(cookieParser());
app.use(express.json());

const url = process.env.MONGODB_URL;
mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.connection.on("connected", function () {
  // console.log("Mongoose default connection open to " + url);
});
mongoose.connection.on("error", function (err) {
  // console.log("Mongoose default connection error: " + err);
});
mongoose.connection.on("disconnected", function () {
  // console.log("Mongoose default connection disconnected");
});

mongoose.connection.once("open", function () {});

app.use("/v1/auth", authRoute);
app.use("/v1/user", userRoute);

app.listen(8000, function () {
  // console.log("Server listening on port 8000");
});

//authentication xác thực authorization phân quyền
