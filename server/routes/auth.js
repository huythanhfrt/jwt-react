const authController = require("../controllers/authController");
const userMiddleware = require("../middleware/userMiddleware");

const authRouter = require("express").Router();

authRouter.post("/register", authController.registerUser);
authRouter.post("/login", authController.loginUser);
authRouter.post("/refresh", authController.requestRefreshToken);
authRouter.post(
  "/logout",
  userMiddleware.verifyToken,
  authController.userLogout
);

module.exports = authRouter;
