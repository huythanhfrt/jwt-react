const userController = require("../controllers/userController");
const userMiddleware = require("../middleware/userMiddleware");

const userRoute = require("express").Router();

userRoute.get("/", userMiddleware.verifyToken, userController.getAllUser);
userRoute.delete(
  "/:id",
  userMiddleware.verifyTokenToDeleteUser,
  userController.deleteUser
);
module.exports = userRoute;
