const jwt = require("jsonwebtoken");

const userMiddleware = {
  verifyToken: (req, res, next) => {
    const token = req.headers.token;
    console.log("req.headers.token: ", req.headers.token);
    if (token) {
      const accessToken = token.split(" ")[1];
      jwt.verify(accessToken, process.env.JWT_ACCESS_KEY, (err, user) => {
        if (err) {
          return res.status(403).json("token is not valid");
        }
        req.user = user;
        next();
      });
    } else {
      return res.status(401).json("you are not authenticated");
    }
  },
  verifyTokenToDeleteUser: (req, res, next) => {
    userMiddleware.verifyToken(req, res, () => {
      if (req.user.id == req.params.id || req.user.admin) {
        // chỉ được xóa chính mình hoặc mình là admin
        next();
      } else {
        return res.status(400).json("you are not allowed to delete");
      }
    });
  },
};

module.exports = userMiddleware;
