const userSchema = require("../models/user");

const userController = {
  getAllUser: async (req, res) => {
    try {
      const userList = await userSchema.find();
      return res.status(200).json(userList);
    } catch (err) {
      return res.status(500).json(err);
    }
  },
  deleteUser: async (req, res) => {
    try {
      const user = await userSchema.findById(req.params.id);
      return res.status(200).json("delete successfully");
    } catch (err) {
      return res.status(500).json(err);
    }
  },
};
module.exports = userController;
