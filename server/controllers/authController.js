const bcrypt = require("bcrypt");
const userSchema = require("../models/user");
const jwt = require("jsonwebtoken");
let refreshTokenList = [];
const authController = {
  registerUser: async (req, res) => {
    try {
      const { username, email, password } = req.body;
      const salt = await bcrypt.genSalt(10);
      const hashed = await bcrypt.hash(password, salt);
      const info = new userSchema({
        username,
        password: hashed,
        email,
      });

      const user = await info.save();
      return res.status(200).json(user);
    } catch (err) {
      return res.status(500).json(err);
    }
  },
  createToken: (user, sign, time) => {
    return jwt.sign(
      {
        id: user.id,
        admin: user.admin,
      },
      sign,
      { expiresIn: time }
    );
  },
  loginUser: async (req, res) => {
    try {
      const { username, password } = req.body;
      const user = await userSchema.findOne({ username });
      const validPassword = await bcrypt.compare(password, user.password);
      if (!user) {
        return res.status(400).json("wrong username");
      }
      if (!validPassword) {
        return res.status(400).json("wrong password");
      }
      if (user && validPassword) {
        const accessToken = authController.createToken(
          user,
          process.env.JWT_ACCESS_KEY,
          "10s"
        );
        const refreshToken = authController.createToken(
          user,
          process.env.JWT_REFRESH_KEY,
          "365d"
        );
        res.cookie("refreshToken", refreshToken, {
          httpOnly: true,
          secure: false,
          path: "/",
          sameSite: "strict",
        });
        refreshTokenList.push(refreshToken);
        const { password, ...info } = user._doc;
        return res.status(200).json({ ...info, accessToken });
      }
    } catch (err) {
      return res.status(500).json(err);
    }
  },
  requestRefreshToken: (req, res) => {
    const refreshToken = req.cookies.refreshToken;
    if (!refreshToken) return res.status(401).json("you are not authenticated");
    if (!refreshTokenList.includes(refreshToken)) {
      return res.status(403).json("token is not valid");
    }
    jwt.verify(refreshToken, process.env.JWT_REFRESH_KEY, (err, user) => {
      if (err) {
        res.status(400).json(err);
      }
      refreshTokenList = [];
      const newAccessToken = authController.createToken(
        user,
        process.env.JWT_ACCESS_KEY,
        "10s"
      );
      const newRefreshToken = authController.createToken(
        user,
        process.env.JWT_REFRESH_KEY,
        "365d"
      );
      refreshTokenList.push(newRefreshToken);

      res.cookie("refreshToken", newRefreshToken, {
        httpOnly: true,
        secure: false,
        path: "/",
        sameSite: "strict",
      });
      return res.status(200).json({ accessToken: newAccessToken });
    });
  },
  userLogout: (req, res) => {
    res.clearCookie("refreshToken");
    refreshTokenList = [];
    return res.status(200).json("logout successfully");
  },
};
module.exports = authController;
