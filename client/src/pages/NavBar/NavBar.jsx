import { useState } from "react";
import { Link, NavLink, useNavigate } from "react-router-dom";
import "../../css/navbar.css";
import { useDispatch, useSelector } from "react-redux";
import { authService } from "../../service/authService";
import { userLogout } from "../../redux/slice/authSlice";
const NavBar = () => {
  const user = useSelector((state) => state.authSlice?.user);
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const handleLogout = async (accessToken) => {
    await authService.logOutUser(accessToken, user?._id);
    dispatch(userLogout());
    navigate("/login");
  };
  return (
    <nav className="navbar-container">
      <Link to="/" className="navbar-home">
        Home
      </Link>
      {user ? (
        <>
          <p className="navbar-user">
            Hi, <span> {user?.username} </span>
          </p>
          <Link
            onClick={() => {
              handleLogout(user?.accessToken);
            }}
            className="navbar-logout"
          >
            Log out
          </Link>
        </>
      ) : (
        <>
          <Link to="/login" className="navbar-login">
            Login
          </Link>
          <Link to="/register" className="navbar-register">
            Register
          </Link>
        </>
      )}
    </nav>
  );
};

export default NavBar;
