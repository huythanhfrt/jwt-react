import { useEffect, useState } from "react";
import "../../css/homepage.css";
import { userService } from "../../service/userService";
import { useDispatch, useSelector } from "react-redux";
import { getAllUserList } from "../../redux/slice/userSlice";
import { useNavigate } from "react-router-dom";
import { authService } from "../../service/authService";
const HomePage = () => {
  //DUMMY DATA
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [msg, setMsg] = useState("");
  let user = useSelector((state) => state.authSlice?.user);
  let userList = useSelector((state) => state.userSlice?.userList);
  const handleDelete = async (id) => {
    try {
      let data = await userService.deleteUser(id, user?.accessToken);
      setMsg(data);
    } catch (error) {
      setMsg(error.response.data);
    }
  };
  const fetchData = async () => {
    if (!user) {
      return navigate("/login");
    }
    let data = await userService.getUserList(user?.accessToken);
    dispatch(getAllUserList(data));
  };
  useEffect(() => {
    fetchData();
  }, []);
  return (
    <div className="homepage">
      <main className="home-container">
        <div className="home-title">User List</div>
        <div className="home-role">
          your role is {user?.admin ? "admin" : "user"}
        </div>
        <div className="home-userlist">
          {userList?.map((user) => {
            return (
              <div key={user.username} className="user-container">
                <div className="home-user">{user.username}</div>
                <div
                  className="delete-user"
                  onClick={() => handleDelete(user._id)}
                >
                  Delete
                </div>
              </div>
            );
          })}
        </div>
        <div>{msg}</div>
      </main>
    </div>
  );
};

export default HomePage;
