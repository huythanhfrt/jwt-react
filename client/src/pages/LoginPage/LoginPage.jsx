import { useState } from "react";
import { useDispatch } from "react-redux";
import "../../css/login.css";
import { Link, useNavigate } from "react-router-dom";
import {
  userLogin,
  userLoginFail,
  userLoginSuccess,
} from "../../redux/slice/authSlice";
import { authService } from "../../service/authService";

const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const handleSubmit = async (e) => {
    e.preventDefault();
    const userInfo = { username, password };
    dispatch(userLogin());
    let data = await authService.loginUser(userInfo);
    if (data) {
      dispatch(userLoginSuccess(data));
      return navigate("/");
    }
  };
  return (
    <section className="login-container">
      <div className="login-title"> Log in</div>
      <form onSubmit={handleSubmit}>
        <label>USERNAME</label>
        <input
          onChange={(e) => setUsername(e.target.value)}
          type="text"
          placeholder="Enter your username"
        />
        <label>PASSWORD</label>
        <input
          onChange={(e) => setPassword(e.target.value)}
          type="password"
          placeholder="Enter your password"
        />
        <button type="submit"> Continue </button>
      </form>
      <div className="login-register"> Don't have an account yet? </div>
      <Link className="login-register-link" to="/register">
        Register one for free
      </Link>
    </section>
  );
};

export default Login;
