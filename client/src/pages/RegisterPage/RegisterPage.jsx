import { useState } from "react";
import "../../css/register.css";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { userLogin } from "../../redux/slice/authSlice";
import { authService } from "../../service/authService";
const Register = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const handleSubmit = async (e) => {
    e.preventDefault();
    const userInfo = { username, password, email };
    dispatch(userLogin());
    let data = await authService.registerUser(userInfo);
    if (data) return navigate("/login");
  };
  return (
    <section className="register-container">
      <div className="register-title"> Sign up </div>
      <form onSubmit={handleSubmit}>
        <label>EMAIL</label>
        <input
          onChange={(e) => setEmail(e.target.value)}
          type="text"
          placeholder="Enter your email"
        />
        <label>USERNAME</label>
        <input
          onChange={(e) => setUsername(e.target.value)}
          type="text"
          placeholder="Enter your username"
        />
        <label>PASSWORD</label>
        <input
          onChange={(e) => setPassword(e.target.value)}
          type="password"
          placeholder="Enter your password"
        />
        <button type="submit"> Create account </button>
      </form>
    </section>
  );
};

export default Register;
