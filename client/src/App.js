import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Login from "./pages/LoginPage/LoginPage";
import HomePage from "./pages/HomePage/HomePage";
import Register from "./pages/RegisterPage/RegisterPage";
import Layout from "./HOC/Layout";
import { route } from "./routes/route";
function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          {route.map((e, i) => {
            return (
              <Route
                key={i}
                path={e.path}
                element={<Layout component={e.component} />}
              />
            );
          })}
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
