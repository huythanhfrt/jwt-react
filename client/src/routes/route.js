import HomePage from "../pages/HomePage/HomePage";
import Login from "../pages/LoginPage/LoginPage";
import Register from "../pages/RegisterPage/RegisterPage";

export const route = [
  {
    path: "/",
    component: <HomePage />,
    exact: true,
    title: "Trang chủ",
  },
  {
    path: "/register",
    component: <Register />,
    exact: true,
    title: "Trang chủ",
  },
  {
    path: "/login",
    component: <Login />,
    exact: true,
    title: "Trang chủ",
  },
];
