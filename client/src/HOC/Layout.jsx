import NavBar from "../pages/NavBar/NavBar";

const Layout = ({ component }) => {
  return (
    <div>
      <NavBar />
      {component}
    </div>
  );
};
export default Layout;
