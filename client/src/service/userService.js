import { httpClient } from "./configUrl";

export const userService = {
  getUserList: (accessToken) => {
    if (!accessToken) return;
    return httpClient.get("/user", {
      headers: {
        token: `Bearer ${accessToken}`,
      },
    });
  },
  deleteUser: (id, accessToken) => {
    return httpClient.delete(`/user/${id}`, {
      headers: {
        token: `Bearer ${accessToken}`,
      },
    });
  },
};
