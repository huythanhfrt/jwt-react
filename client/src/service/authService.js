import { httpClient } from "./configUrl";

export const authService = {
  registerUser: (userInfo) => {
    return httpClient.post("/auth/register", userInfo);
  },
  loginUser: (userInfo) => {
    return httpClient.post("/auth/login", userInfo);
  },
  refreshTokenUser: () => {
    return httpClient.post("/auth/refresh");
  },
  logOutUser: (accessToken, id) => {
    const token = `Bearer ${accessToken}`;
    return httpClient.post("/auth/logout", id, {
      headers: {
        token,
      },
    });
  },
};
