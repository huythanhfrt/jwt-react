import axios from "axios";
import jwtDecode from "jwt-decode";
import { authService } from "./authService";
import { store } from "../redux/store";
import { userLoginSuccess } from "../redux/slice/authSlice";
const url = "http://localhost:8000/v1";
export const httpClient = axios.create({
  baseURL: url,
  withCredentials: true,
});
httpClient.interceptors.request.use(
  async function (config) {
    // const token = config.headers?.token?.split(" ")[1];
    // if (token) {
    //   const userToken = jwtDecode(token);
    //   console.log("userToken: ", userToken);
    //   const date = new Date().getTime() / 1000;
    //   if (userToken?.exp < date) {
    //     let data = await authService.refreshTokenUser();
    //     let user = store.getState().authSlice.user;
    //     console.log("user: ", user);
    //     store.dispatch(
    //       userLoginSuccess({ ...user, accessToken: data.accessToken })
    //     );
    //   }
    // }
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);
// Add a response interceptor
httpClient.interceptors.response.use(
  function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response.data;
  },
  async function (error) {
    const token = error.config.headers?.token;
    switch (error.response.status) {
      case 401:
      case 403:
        if (token) {
          const userToken = jwtDecode(token);
          const date = new Date().getTime() / 1000;
          if (userToken?.exp < date) {
            let data = await authService.refreshTokenUser();
            let user = store.getState().authSlice.user;
            store.dispatch(
              userLoginSuccess({ ...user, accessToken: data.accessToken })
            );
          }
        }
      default:
        break;
    }
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
  }
);
