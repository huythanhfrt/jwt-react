import { createSlice } from "@reduxjs/toolkit";

let initialState = {
  userList: null,
  isLoading: false,
  error: false,
};
const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    getAllUserList: (state, { payload }) => {
      state.isLoading = false;
      state.userList = payload;
      state.error = false;
    },
    userLoginFail: (state) => {
      state.isLoading = false;
      state.error = true;
    },
  },
});
export const { getAllUserList } = userSlice.actions;
export default userSlice.reducer;
