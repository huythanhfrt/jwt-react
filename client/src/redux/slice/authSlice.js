import { createSlice } from "@reduxjs/toolkit";

let initialState = {
  user: null,
  isLoading: false,
  error: false,
};

const authSlice = createSlice({
  name: "authSlice",
  initialState,
  reducers: {
    userLogin: (state) => {
      state.isLoading = true;
    },
    userLoginSuccess: (state, { payload }) => {
      state.isLoading = false;
      state.user = payload;
      state.error = false;
    },
    userLoginFail: (state) => {
      state.isLoading = false;
      state.error = true;
    },
    userLogout: (state) => {
      state.user = null;
    },
  },
});
export const { userLogin, userLoginSuccess, userLoginFail, userLogout } =
  authSlice.actions;
export default authSlice.reducer;
